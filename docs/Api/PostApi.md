# Ensi\EnsiTaskClient\PostApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPost**](PostApi.md#createPost) | **POST** /posts/posts | Создание объекта Post
[**deletePost**](PostApi.md#deletePost) | **DELETE** /posts/posts/{id} | Удаление объекта Post
[**getPost**](PostApi.md#getPost) | **GET** /posts/posts/{id} | Получение объекта Post
[**patchPost**](PostApi.md#patchPost) | **PATCH** /posts/posts/{id} | Обновление объекта Post
[**searchPosts**](PostApi.md#searchPosts) | **POST** /posts/posts:search | Поиск объектов типа Post



## createPost

> \Ensi\EnsiTaskClient\Dto\PostResponse createPost($create_post_request)

Создание объекта Post

Создание объекта Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\EnsiTaskClient\Api\PostApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_post_request = new \Ensi\EnsiTaskClient\Dto\CreatePostRequest(); // \Ensi\EnsiTaskClient\Dto\CreatePostRequest | 

try {
    $result = $apiInstance->createPost($create_post_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostApi->createPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_post_request** | [**\Ensi\EnsiTaskClient\Dto\CreatePostRequest**](../Model/CreatePostRequest.md)|  |

### Return type

[**\Ensi\EnsiTaskClient\Dto\PostResponse**](../Model/PostResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deletePost

> \Ensi\EnsiTaskClient\Dto\EmptyDataResponse deletePost($id)

Удаление объекта Post

Удаление объекта Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\EnsiTaskClient\Api\PostApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deletePost($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostApi->deletePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\EnsiTaskClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getPost

> \Ensi\EnsiTaskClient\Dto\PostResponse getPost($id, $include)

Получение объекта Post

Получение объекта Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\EnsiTaskClient\Api\PostApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getPost($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostApi->getPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\EnsiTaskClient\Dto\PostResponse**](../Model/PostResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchPost

> \Ensi\EnsiTaskClient\Dto\PostResponse patchPost($id, $patch_post_request)

Обновление объекта Post

Обновление части полей объекта Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\EnsiTaskClient\Api\PostApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_post_request = new \Ensi\EnsiTaskClient\Dto\PatchPostRequest(); // \Ensi\EnsiTaskClient\Dto\PatchPostRequest | 

try {
    $result = $apiInstance->patchPost($id, $patch_post_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostApi->patchPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_post_request** | [**\Ensi\EnsiTaskClient\Dto\PatchPostRequest**](../Model/PatchPostRequest.md)|  |

### Return type

[**\Ensi\EnsiTaskClient\Dto\PostResponse**](../Model/PostResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchPosts

> \Ensi\EnsiTaskClient\Dto\SearchPostsResponse searchPosts($search_posts_request)

Поиск объектов типа Post

Поиск объектов типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\EnsiTaskClient\Api\PostApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_posts_request = new \Ensi\EnsiTaskClient\Dto\SearchPostsRequest(); // \Ensi\EnsiTaskClient\Dto\SearchPostsRequest | 

try {
    $result = $apiInstance->searchPosts($search_posts_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostApi->searchPosts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_posts_request** | [**\Ensi\EnsiTaskClient\Dto\SearchPostsRequest**](../Model/SearchPostsRequest.md)|  |

### Return type

[**\Ensi\EnsiTaskClient\Dto\SearchPostsResponse**](../Model/SearchPostsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

