# Ensi\EnsiTaskClient\VoteApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createVote**](VoteApi.md#createVote) | **POST** /posts/votes | Создание объекта Vote
[**deleteVote**](VoteApi.md#deleteVote) | **DELETE** /posts/votes/{id} | Удаление объекта Vote
[**patchVote**](VoteApi.md#patchVote) | **PATCH** /posts/votes/{id} | Обновление объекта Vote
[**searchVotes**](VoteApi.md#searchVotes) | **POST** /posts/votes:search | Поиск объектов Vote



## createVote

> \Ensi\EnsiTaskClient\Dto\VoteResponse createVote($create_vote_request)

Создание объекта Vote

Создание объекта типа Vote

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\EnsiTaskClient\Api\VoteApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_vote_request = new \Ensi\EnsiTaskClient\Dto\CreateVoteRequest(); // \Ensi\EnsiTaskClient\Dto\CreateVoteRequest | 

try {
    $result = $apiInstance->createVote($create_vote_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VoteApi->createVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_vote_request** | [**\Ensi\EnsiTaskClient\Dto\CreateVoteRequest**](../Model/CreateVoteRequest.md)|  |

### Return type

[**\Ensi\EnsiTaskClient\Dto\VoteResponse**](../Model/VoteResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteVote

> \Ensi\EnsiTaskClient\Dto\EmptyDataResponse deleteVote($id)

Удаление объекта Vote

Удаление объекта Vote

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\EnsiTaskClient\Api\VoteApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteVote($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VoteApi->deleteVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\EnsiTaskClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchVote

> \Ensi\EnsiTaskClient\Dto\VoteResponse patchVote($id, $patch_vote_request)

Обновление объекта Vote

Обновление части полей объекта Vote

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\EnsiTaskClient\Api\VoteApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_vote_request = new \Ensi\EnsiTaskClient\Dto\PatchVoteRequest(); // \Ensi\EnsiTaskClient\Dto\PatchVoteRequest | 

try {
    $result = $apiInstance->patchVote($id, $patch_vote_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VoteApi->patchVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_vote_request** | [**\Ensi\EnsiTaskClient\Dto\PatchVoteRequest**](../Model/PatchVoteRequest.md)|  |

### Return type

[**\Ensi\EnsiTaskClient\Dto\VoteResponse**](../Model/VoteResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchVotes

> \Ensi\EnsiTaskClient\Dto\SearchVotesResponse searchVotes($search_votes_request)

Поиск объектов Vote

Поиск объектов Vote

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\EnsiTaskClient\Api\VoteApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_votes_request = new \Ensi\EnsiTaskClient\Dto\SearchVotesRequest(); // \Ensi\EnsiTaskClient\Dto\SearchVotesRequest | 

try {
    $result = $apiInstance->searchVotes($search_votes_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VoteApi->searchVotes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_votes_request** | [**\Ensi\EnsiTaskClient\Dto\SearchVotesRequest**](../Model/SearchVotesRequest.md)|  |

### Return type

[**\Ensi\EnsiTaskClient\Dto\SearchVotesResponse**](../Model/SearchVotesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

