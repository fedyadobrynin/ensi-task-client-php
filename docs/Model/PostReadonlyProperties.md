# # PostReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор поста | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | [optional] 
**rating** | **int** | Рейтинг поста (положительный или отрицательный) | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


