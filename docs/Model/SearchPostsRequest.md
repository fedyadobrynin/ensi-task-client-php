# # SearchPostsRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sort** | **string[]** |  | [optional] 
**filter** | [**\Ensi\EnsiTaskClient\Dto\SearchPostsFilter**](SearchPostsFilter.md) |  | [optional] 
**include** | **string[]** |  | [optional] 
**pagination** | [**\Ensi\EnsiTaskClient\Dto\RequestBodyPagination**](RequestBodyPagination.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


