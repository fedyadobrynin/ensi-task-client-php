# # SearchPostsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Ensi\EnsiTaskClient\Dto\Post[]**](Post.md) |  | 
**meta** | [**\Ensi\EnsiTaskClient\Dto\SearchPostsResponseMeta**](SearchPostsResponseMeta.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


